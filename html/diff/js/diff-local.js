/* Install QueryString jQuery "plugin"
 * http://stackoverflow.com/a/3855394/1094484 (BrunoLM)
 */
(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=', 2);
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

/* Alert functions
 * Via the amazing bootstrap-notify */
function errorAlert(msg) {
    $.notify({
        title: '<strong>Error:</strong>',
        message: msg
    },{
        type: 'danger',
        animate: {
            enter:  'animated fadeInRight',
            exit:  'animated fadeOutRight'
        },
        showProgressbar: false,
        delay: 0
    });
}

/* Create progress bar alert
 *   progAlert(title, msg, status, progress)
 *     status: Bootstrap color classes (e.g. info, danger, etc.)
 *     prorgress: 0-100
 * 
 * This returns the notification object, which you can either
 * update directly (by calling the bootstrap-notify API), or
 * with the update helper functions below.
 */
function progAlert(title, msg, status, progress) {
    var notify = $.notify('<strong>' + title + '</strong><br />' + msg, {
        type: status,
        animate: {
            enter:  'animated fadeInRight',
            exit:  'animated fadeOutRight'
        },
        allow_dismiss: false,
        showProgressbar: true,
        delay: 0
    });

    return notify;
}

/* Update progress bar alert
 *   progAlertUpdate(notifyObj, title, msg, status, progress)
 *     notifyObj: the notification object created from progAlert
 *     status: Bootstrap color classes (e.g. info, danger, etc.)
 *     prorgress: 0-100
 * 
 * This updates the current progress bar alert shown.
 */
function progAlertUpdate(notifyObj, title, msg, status, progress) {
    notifyObj.update(
        {
            'type' : status,
            'message': '<strong>' + title + '</strong><br />' + msg,
            'progress' : progress,
        }
    );
}

/* Update progress bar alert title and message
 *   progAlertUpdateMsg(notifyObj, title, msg)
 *     notifyObj: the notification object created from progAlert
 * 
 * This updates the current progress bar alert shown.
 */
function progAlertUpdateMsg(notifyObj, title, msg) {
    notifyObj.update(
        {
            'message': '<strong>' + title + '</strong><br />' + msg,
        }
    );
}

/* Update progress bar status
 *   progAlertUpdateMsg(notifyObj, status)
 *     notifyObj: the notification object created from progAlert
 *     status: Bootstrap color classes (e.g. info, danger, etc.)
 * 
 * This updates the current progress bar alert shown.
 */
function progAlertUpdateStatus(notifyObj, status) {
    notifyObj.update(
        {
            'type' : status,
        }
    );
}

/* Update progress bar alert title, message, and progress
 *   progAlertUpdateMsg(notifyObj, title, msg, progress)
 *     notifyObj: the notification object created from progAlert
 *     prorgress: 0-100
 * 
 * This updates the current progress bar alert shown.
 */
function progAlertUpdateMsgAndProgress(notifyObj, title, msg, progress) {
    notifyObj.update(
        {
            'message': '<strong>' + title + '</strong><br />' + msg,
            'progress' : progress,
        }
    );
}

/* Update progress bar alert progress
 *   progAlertUpdateMsg(notifyObj, progress)
 *     notifyObj: the notification object created from progAlert
 *     prorgress: 0-100
 * 
 * This updates the current progress bar alert shown.
 */
function progAlertUpdateProgress(notifyObj, progress) {
    notifyObj.update(
        {
            'progress' : progress,
        }
    );
}

/* Close progress bar alert
 *   progAlertUpdateMsg(notifyObj, delay=0)
 *     notifyObj: the notification object created from progAlert
 *     delay: number of ms to wait before closing. (1000 ms = 1 second)
 * 
 * This closes the current progress bar alert shown.
 */
function progDone(notifyObj, delay=0) {
    setTimeout(function() { notifyObj.close(); }, delay);
}

/* Perform diff!
 *   doComparison(json1, json2, src1 = "", src2 = "")
 *     json1: first JSON string or JS object
 *     json2: second JSON string or JS object
 *     src1: first source name (e.g. a filename). Default empty.
 *     src2: second source name (e.g. a filename). Default empty.
 * 
 * Performs a diff on json1 and json2. Can use either a JSON string
 * or JS object for comparison - native library (jsondiffpath)
 * uses JS objects.
 */
function doComparison(json1, json2, src1 = "", src2 = "") {
    if (typeof json1 !== typeof json2) {
        errorAlert("doComparison was called with two different types!<br />Types given: " + (typeof json1) + " vs. " + (typeof json2));
        return false;
    }
    
    if (typeof json1 === 'object') {
        // No parse needed
        final_json1 = json1;
        final_json2 = json2;
        
        // Stringify needed, though!
        try {
            final_json1_str = JSON.stringify(json1, null, 2);
            final_json2_str = JSON.stringify(json2, null, 2);
        } catch(e) {
            errorAlert("Comparison failed when trying to stringify JSON!<br />Types given: " + (typeof json1) + " vs. "
                + (typeof json2) + "<br />Error: " + e);
            return false;
        }
    } else if (typeof json1 === 'string') {
        // We need to parse
        try {
            final_json1 = JSON.parse(json1, null, 2);
            final_json2 = JSON.parse(json2, null, 2);
        } catch(e) {
            errorAlert("Comparison failed when trying to parse JSON!<br />Types given: " + (typeof json1) + " vs. "
                + (typeof json2) + "<br />Error: " + e);
            return false;
        }
        
        // No need to stringify
        final_json1_str = json1;
        final_json2_str = json2;
    } else {
        errorAlert("doComparison was called with invalid types!<br />Only types allowed are serializable JSON objects, and strings.<br />Types given: " + (typeof json1) + " vs. " + (typeof json2));
        return false;
    }
    
    $('#json-input-left-filename').html(src1);
    $('#json-input-right-filename').html(src2);
    
    $('#json-input-left').val(final_json1_str);
    $('#json-input-right').val(final_json2_str);
    
    try {
        var delta = jsondiffpatch.diff(final_json1, final_json2);
        $('#visualdiff').html(jsondiffpatch.formatters.html.format(delta, final_json1));
        $('#annotateddiff').html(jsondiffpatch.formatters.annotated.format(delta, final_json1));
    } catch(e) {
        errorAlert("Comparison failed when trying to perform difference! Types given: " + (typeof json1) + " vs. "
                + (typeof json2) + "<br />Error: " + e);
        return false;
    }
    
    return true;
}

function lockCompareBtn(toggle) {
    $('button').prop('disabled', toggle);
}

/* Perform diff on two JSON URLs!
 *   doAJAXComparison(json_loc_1, json_loc_2)
 *     json_loc_1: first JSON URL to compare with
 *     json_loc_2: second JSON URL to compare with
 * 
 * Performs a diff on json_loc_1 and json_loc_1. JSON is
 * loaded from the two URLs, then compared with the above
 * doComparison().
 */
function doAJAXComparison(json_loc_1, json_loc_2) {
    var success1 = false;
    var success2 = false;
    
    lockCompareBtn(true);
    
    $('#json-input-left-filename').html("(Loading " + json_loc_1 + "...)");
    $('#json-input-right-filename').html("(Loading " + json_loc_2 + "...)");
    
    x = progAlert("Loading comparison...", "Loading first file... (" + json_loc_1 + ")", "info", 25);
    
    ajaxJQHandlerRegister(json_loc_1, null, function(http_code) {
        // Handle error accordingly
        // 200 = we loaded it successfully, but JSON parsing failed
        progDone(x);
        $('#json-input-left-filename').html("Could not load: " + json_loc_1);
        $('#json-input-right-filename').html("Skipped: " + json_loc_2);
        if (http_code == 200) {
            errorAlert("Unable to load first file for comparison (" + json_loc_1 + ")!<br />The JSON seems to be invalid.");
        } else {
            errorAlert("Unable to load first file for comparison (" + json_loc_1 + ")!<br />The path seems to be invalid. (HTTP: " + http_code + ")");
        }
        lockCompareBtn(false);
    });
    
    $.getJSON(json_loc_1, function(data1) {
        success1 = true;
        progAlertUpdateMsgAndProgress(x, "Loading comparison...", "Loading second file... (" + json_loc_2 + ")", 50);
        console.log(json_loc_1 + " loaded, loading 2nd one...");
        
        ajaxJQHandlerRegister(json_loc_2, null, function(http_code) {
            // Handle error accordingly
            // 200 = we loaded it successfully, but JSON parsing failed
            progDone(x);
            $('#json-input-left-filename').html("Skipped: " + json_loc_1);
            $('#json-input-right-filename').html("Could not load: " + json_loc_2);
            if (http_code == 200) {
                errorAlert("Unable to load second file for comparison (" + json_loc_2 + ")!<br />The JSON seems to be invalid.");
            } else {
                errorAlert("Unable to load second file for comparison (" + json_loc_2 + ")!<br />The path seems to be invalid. (HTTP: " + http_code + ")");
            }
            lockCompareBtn(false);
        });
        
        $.getJSON(json_loc_2, function(data2) {
            success2 = true;
            progAlertUpdateMsgAndProgress(x, "Loading comparison...", "Running comparison...", 75);
            console.log(json_loc_2 + " loaded, creating and showing diff...");
            
            res = doComparison(data1, data2, json_loc_1, json_loc_2);
            
            if (res) {
                progAlertUpdate(x, "Comparison Complete", "Successfully performed comparison!", "success", 100);
                progDone(x, 5000);
            } else {
                progDone(x);
            }
            
            lockCompareBtn(false);
        });
    });
}

/* Connect compare button to doing a text-based comparison */
$('#comparebtn').click(function() {
    lockCompareBtn(true);
    
    x = progAlert("Performing comparison...", "Computing comparison...", "info", 0);
    
    res = doComparison($('#json-input-left').val(), $('#json-input-right').val());
    if (res) {
        progAlertUpdate(x, "Comparison Complete", "Successfully performed comparison!", "success", 100);
        progDone(x, 5000);
    } else {
        progDone(x);
    }
    
    lockCompareBtn(false);
});

/* If we have in the url ?a=bla1&b=bla2, do an AJAX comparison
 * automatically.
 */
if ($.QueryString["a"] && $.QueryString["b"]) {    
    doAJAXComparison($.QueryString["a"], $.QueryString["b"]);
}
