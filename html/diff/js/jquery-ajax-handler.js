/*
 * jQuery AJAX Handler
 * Just a silly AJAX handler for jQuery when all else fails
 * (e.g. jQuery's getJSON())
 *
 * By Albert Huang (https://github.com/alberthdev)
 * 
 * Hook installation based on this suggestion:
 * http://stackoverflow.com/a/5553540/1094484
 * 
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* Stub for console.log
 * http://stackoverflow.com/a/4834126/1094484 (Henrik Joreteg)
 */
window.console||(console={log:function(){}});

/* Error handler for jQuery AJAX (since .getJSON doesn't have it)
 * 
 * Usage:
 *   ajaxJQHandlerRegister(path, callback_success, callback_error)
 *   
 *   callback_success(data):
 *     data: resulting data
 *   
 *   callback_error(httpStatus):
 *     httpStatus: numerical HTTP failure status (e.g. 404)
 * 
 * Note that this does not distinguish itself from the same paths!
 * That said, it will process then in the order they are received.
 */
var ajaxJQHandlerHookPaths = [];
var ajaxJQHandlerHookErrorFuncs = [];
var ajaxJQHandlerHookSuccessFuncs = [];

function ajaxJQErrorHandler(jqXHR, textStatus, errorThrown) {
    searchIndex = ajaxJQHandlerHookPaths.indexOf(jqXHR.requestURL);
    if (searchIndex != -1) {
        // Remove entry and make callback!
        console.log("ajaxJQErrorHandler [" + jqXHR.requestURL + "]: we're handling this!");
        console.log("ajaxJQErrorHandler [" + jqXHR.requestURL + "]: jqXHR.status = " + jqXHR.status);
        console.log("ajaxJQErrorHandler [" + jqXHR.requestURL + "]: textStatus = " + textStatus);
        console.log("ajaxJQErrorHandler [" + jqXHR.requestURL + "]: errorThrown = " + errorThrown);
        ajaxJQHandlerHookPaths.splice(searchIndex, 1);
        ajaxJQHandlerHookSuccessFuncs.splice(searchIndex, 1);
        callback = ajaxJQHandlerHookErrorFuncs.splice(searchIndex, 1)[0];
        console.log("ajaxJQErrorHandler [" + jqXHR.requestURL + "]: callback = " + callback);
        if (typeof callback === "function") {
            callback(jqXHR.status);
        }
    }
}

function ajaxJQSuccessHandler(data, textStatus, jqXHR) {
    searchIndex = ajaxJQHandlerHookPaths.indexOf(jqXHR.requestURL);
    if (searchIndex != -1) {
        // Remove entry and make callback!
        console.log("ajaxJQSuccessHandler [" + jqXHR.requestURL + "]: we're handling this!");
        console.log("ajaxJQSuccessHandler [" + jqXHR.requestURL + "]: jqXHR.status = " + jqXHR.status);
        console.log("ajaxJQSuccessHandler [" + jqXHR.requestURL + "]: textStatus = " + textStatus);
        console.log("ajaxJQSuccessHandler [" + jqXHR.requestURL + "]: errorThrown = " + errorThrown);
        ajaxJQHandlerHookPaths.splice(searchIndex, 1);
        ajaxJQHandlerHookErrorFuncs.splice(searchIndex, 1);
        callback = ajaxJQHandlerHookSuccessFuncs.splice(searchIndex, 1)[0];
        console.log("ajaxJQErrorHandler [" + jqXHR.requestURL + "]: callback = " + callback);
        if (typeof callback === "function") {
            callback(data);
        }
    }
}

function ajaxJQHandlerRegister(path, callback_success, callback_error) {
    console.log("ajaxJQHandlerRegister [" + path + "]: registering handler for this path!");
    ajaxJQHandlerHookPaths.push(path);
    ajaxJQHandlerHookSuccessFuncs.push(callback_success);
    ajaxJQHandlerHookErrorFuncs.push(callback_error);
}

/* Install error handler! */
$.ajaxSetup({
      "error": ajaxJQErrorHandler,
      "success": ajaxJQSuccessHandler,
      "beforeSend": function(jqxhr, settings) { jqxhr.requestURL = settings.url; },
});
