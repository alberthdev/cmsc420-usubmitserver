/* http://stackoverflow.com/a/13067009/1094484
 * By Ian Clark, w/ some modifications by Albert Huang
 */
(function(document, history, location) {
  // this is annoying, disable
  var HISTORY_SUPPORT = false; //!!(history && history.pushState);

  var anchorScrolls = {
    ANCHOR_REGEX: /^#[^ ]+$/,
    OFFSET_HEIGHT_PX: 50,

    /**
     * Establish events, and fix initial scroll position if a hash is provided.
     */
    init: function() {
      this.scrollToCurrent();
      $(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
      $('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
    },

    /**
     * Return the offset amount to deduct from the normal scroll position.
     * Modify as appropriate to allow for dynamic calculations
     */
    getFixedOffset: function() {
      return this.OFFSET_HEIGHT_PX;
    },

    /**
     * If the provided href is an anchor which resolves to an element on the
     * page, scroll to it.
     * @param  {String} href
     * @return {Boolean} - Was the href an anchor.
     */
    scrollIfAnchor: function(href, pushToHistory) {
      var match, anchorOffset;
      console.log("Got sketchy href: " + href);
      console.log("window.location = " + window.location);
      console.log("window.location.hash = " + window.location.hash);

      if(!this.ANCHOR_REGEX.test(href)) {
        console.log("Got sketchy href: " + href);
        if ((href != "#") && (href !== null)) {
            console.log("FAIL: " + href);
            console.log(href);
            return false;
        } else {
          match = document.body;
        }
      } else {
          match = document.getElementById(href.slice(1));
      }

      if(match) {
        anchorOffset = $(match).offset().top - this.getFixedOffset();
        $('html, body').animate({ scrollTop: anchorOffset});

        // Add the state to history as-per normal anchor links
        if(HISTORY_SUPPORT && pushToHistory) {
          history.pushState({}, document.title, location.pathname + href);
        }
      }

      return !!match;
    },
    
    /**
     * Attempt to scroll to the current location's hash.
     */
    scrollToCurrent: function(e) { 
      if(this.scrollIfAnchor(window.location.hash) && e) {
      	e.preventDefault();
      }
    },

    /**
     * If the click event's target was an anchor, fix the scroll position.
     */
    delegateAnchors: function(e) {
      var elem = e.target;
      console.log("Element " + elem.tagName + " has href = " + elem.getAttribute('href'));
      
      // This event tends to find spans or divs...
      // Find the nearest parent <a> tag, and use that instead!
      if (elem.tagName != "a") {
          elem = elem.closest("a");
          console.log("Attempt to find new: Element " + elem.tagName + " has href = " + elem.getAttribute('href'));
      }
      window.lookatthis = elem;
      window.lookatthis2 = e;
      if(this.scrollIfAnchor(elem.getAttribute('href'), true)) {
        e.preventDefault();
      }
    }
  };

  $(document).ready($.proxy(anchorScrolls, 'init'));
})(window.document, window.history, window.location);
