#!/usr/bin/env python
import os
import sys

import json
import xml2d

def read_file_to_str(filename):
    try:
        fh = open(filename, "rb")
        result = fh.read()
        fh.close()
    except IOError as e:
        errno, strerror = e.args
        print("ERROR: Could not read result file: %s" % sys.argv[1])
        print("       I/O Error %d: %s" % (errno, strerror))
    except:
        print("ERROR: Unexpected error:", sys.exc_info()[0])
        raise
    return result

def parse_args():
    if len(sys.argv) != 2:
        print("Usage: %s [result file]" % sys.argv[0])
        print("This converts XML to JSON. Output will be RESULT_FILE_NAME.json.")
        sys.exit(1)
    
    return sys.argv[1]

def main():
    filename = parse_args()
    try:
        result = read_file_to_str(filename)
    except IOError as e:
        errno, strerror = e.args
        print("ERROR: Could not read file: %s" % filename)
        print("       I/O Error %d: %s" % (errno, strerror))
    except:
        print("ERROR: Unexpected error:", sys.exc_info()[0])
        raise
    
    raw_json = json.dumps((xml2d.convert_xml_to_dict(result)))
    
    fh_json = open(filename + ".json", "w")
    fh_json.write(raw_json)
    fh_json.close()
    
    sys.exit(0)

if __name__ == "__main__":
    main()
