#!/bin/bash
trap kill_all_bg SIGINT

# One-liner to get current script directory!
# http://stackoverflow.com/a/246128/1094484
CDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Make ../results if it doesn't exist already
[ ! -d ../results/ ] && mkdir ../results/

if [ -z $JAR_FILE ];then
    # Try to autodetect the correct jar file
    JAR_FILE=`cat ../.gitlab-ci.yml | grep '^[[:space:]]*JAR_FILE' | sed -e 's/^[ \t]*//' | cut -f2 -d' ' | grep -o '^[^#]*'`
    
    if [ ! -f ../bin/$JAR_FILE ];then
        echo "ERROR: JAR file $JAR_FILE not found. JAR must be built before"
        echo "       testing anything."
        echo ""
        echo "       Alternatively, since the JAR_FILE environment variable was"
        echo "       not set, autodetection of the JAR file may have failed."
        echo "       (Detected: $JAR_FILE)"
        exit 1
    fi
fi

if [ ! -f ../bin/$JAR_FILE ];then
    echo "ERROR: JAR file $JAR_FILE not found. JAR must be built before"
    echo "       testing anything."
    exit 1
fi

echo "Running tests with JAR: ../bin/$JAR_FILE"
echo "Display variable (if any): $DISPLAY"

pidtree() (
    [ -n "$ZSH_VERSION"  ] && setopt shwordsplit
    declare -A CHILDS
    while read P PP;do
        CHILDS[$PP]+=" $P"
    done < <(ps -e -o pid= -o ppid=)

    walk() {
        for i in ${CHILDS[$1]};do
            echo $i
            walk $i
        done
    }

    for i in "$@";do
        walk $i
    done
)

kill_subprocs() {
    for sub_pid in `pidtree $1`; do
        kill -s 0 $sub_pid 2>/dev/null || continue
        kill -SIGINT $sub_pid 2>/dev/null
        sleep 1s
        kill -s 0 $sub_pid 2>/dev/null || continue
        kill -9 $sub_pid 2>/dev/null
    done
}

kill_all_bg() {
    echo "[kill_all_bg] Killing all background processes!"
    BG_PROCS=$(jobs -p)
    for bg_proc in $BG_PROCS; do
        kill_subprocs $bg_proc
    done
    kill $(jobs -p) 2>/dev/null
    exit 0
}

delaykill() {
    sleep 30s
    kill -s 0 $1 2>/dev/null || return
    echo "[delaykill] Timer reached, stopping PID and allow it to terminate (5s): $1"
    kill -s 0 $1 2>/dev/null && kill $1 2>/dev/null
    sleep 5s
    kill -s 0 $1 2>/dev/null || return
    echo "[delaykill] Grace timer reached, killing PID: $1"
    kill -s 0 $1 2>/dev/null && kill -9 $1 2>/dev/null
}

cd ../tests/
for tfile in *.input.xml; do
    btfile=`basename $tfile`
    echo "Testing: $btfile"
    
    rm -f *.png *.html
    
    java -jar ../bin/$JAR_FILE < $tfile &> ../results/$btfile.ci.output.xml &
    export TEST_PROC=$!
    delaykill $TEST_PROC &
    export WATCHDOG_PROC=$!
    wait $TEST_PROC
    cerr=$?
    kill_subprocs $WATCHDOG_PROC
    kill -INT $WATCHDOG_PROC 2>/dev/null
    
    if [ ! "$cerr" = "0" ]; then
        echo " -> Test failed!"
        echo "$cerr" > ../results/$btfile.ci.error
    fi
    
    for img in *.png; do
        [ "$img" = "*.png" ] && break
        echo " -> Collecting image: $img -> $btfile.ci.imgs/"
        mkdir -p "../results/$btfile.ci.imgs"
        cp "$img" "../results/$btfile.ci.imgs/"
    done
    
    for html in *.html; do
        [ "$html" = "*.html" ] && break
        echo " -> Collecting generated webpage: $html -> $btfile.ci.pages/"
        mkdir -p "../results/$btfile.ci.pages"
        cp "$html" "../results/$btfile.ci.pages/"
    done
done
cd "$CDIR"

kill_all_bg
