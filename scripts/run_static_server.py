#!/usr/bin/env python
import os
import sys
import http.server
import socketserver

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
PUBLIC_DIR = os.path.join(os.path.dirname(SCRIPT_DIR), "public")
PORT = 8000

Handler = http.server.SimpleHTTPRequestHandler

# First, try to change to the correct directory.
try:
    os.chdir(PUBLIC_DIR)
except FileNotFoundError:
    print(" ERR: Can't find public directory! Make sure you run the static")
    print("      website generation scripts first before running this server.")
    sys.exit(1)

# Attempt to bind port...
while 1:
    try:
        print("INFO: Trying to bind to port: %d" % PORT)
        httpd = socketserver.TCPServer(("", PORT), Handler)
        break
    except OSError:
        print("WARN: Port %d seems to be in use! Trying a new one..." % PORT)
        PORT += 1
        
        if PORT > 65535:
            print(" ERR: The limit has been reached on the available ports!" % PORT)

print("INFO: Web server is on port %d!" % PORT)
print("      Open http://127.0.0.1:%d/ to see the server!" % PORT)
httpd.serve_forever()
