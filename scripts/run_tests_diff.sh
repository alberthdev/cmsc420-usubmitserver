#!/bin/bash
echo "Running diff tests..."

# Make ../results if it doesn't exist already
[ ! -d ../results/ ] && mkdir ../results/

for tfile in ../tests/*.input.xml; do
    btfile=`basename $tfile`
    echo "Diff Testing: $btfile"
    
    nfile=`printf $btfile | sed s/input/output/g`
    echo " -> Converting to JSON: $btfile.ci.output.xml"
    python3 xml_to_json.py ../results/$btfile.ci.output.xml &>/dev/null
    echo " -> Converting to JSON: $nfile"
    python3 xml_to_json.py ../tests/$nfile &>/dev/null
    mv ../tests/$nfile.json ../results/
    echo " -> Running diff..."
    python3 diff_xml.py ../results/$btfile.ci.output.xml ../tests/$nfile &> ../results/$btfile.ci.diff
    cerr=$?
    
    if [ ! "$cerr" = "0" ]; then
        echo " -> Diff test failed!"
        echo "$cerr" > ../results/$btfile.ci.errordiff
    fi
done
