#!/usr/bin/env python
import os
import collections
from lxml import etree

def convert_ele_to_dict(input_element):
    pydict = collections.OrderedDict()
    pydict["&name"] = input_element.tag
    pydict["&text"] = input_element.text
    pydict["&tail"] = input_element.tail
    
    if pydict["&text"]:
        pydict["&text"] = pydict["&text"].strip()
        if pydict["&text"] == "":
            pydict["&text"] = None
    
    if pydict["&tail"]:
        pydict["&tail"] = pydict["&tail"].strip()
        if pydict["&tail"] == "":
            pydict["&tail"] = None
    
    if len(input_element):
        pydict["&children"] = []
        for e in input_element:
            pydict["&children"].append(convert_ele_to_dict(e))
    pydict.update(input_element.attrib)
    
    return pydict

def convert_xml_to_dict(input_xml):
    if (os.path.isfile(input_xml)):
        tree = etree.parse(input_xml)
        tree = tree.getroot()
    else:
        tree = etree.fromstring(input_xml)
    return convert_ele_to_dict(tree)
