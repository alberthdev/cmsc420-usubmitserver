#!/usr/bin/env python
import os
import sys

import json
import xml2d
from deepdiff import DeepDiff
from pprint import pprint

class XmlDiff(object):
    def __init__(self, xml1, xml2):
        self.dict1 = json.loads(json.dumps((xml2d.convert_xml_to_dict(xml1))))
        self.dict2 = json.loads(json.dumps((xml2d.convert_xml_to_dict(xml2))))

    def equal(self):
        return self.dict1 == self.dict2
    
    def diff(self):
        return DeepDiff(self.dict2, self.dict1)

def read_file_to_str(filename):
    result = None
    try:
        fh = open(filename, "rb")
        result = fh.read()
        fh.close()
    except IOError as e:
        errno, strerror = e.args
        print("ERROR: Could not read result file: %s" % filename)
        print("       I/O Error %d: %s" % (errno, strerror))
    except:
        print("ERROR: Unexpected error:", sys.exc_info()[0])
        raise
    return result

def parse_args():
    if len(sys.argv) != 3:
        print("Usage: %s [result file] [correct file]" % sys.argv[0])
        print("Output will be the resulting difference, if any.")
        print("If there is a difference, this will return 1. Otherwise, 0.")
        sys.exit(1)
    
    result = read_file_to_str(sys.argv[1])
    correct = read_file_to_str(sys.argv[2])
    
    if (result is None) or (correct is None):
        sys.exit(1)
    
    return (result, correct)

def main():
    result, correct = parse_args()
    
    diff = XmlDiff(result, correct)
    eq = diff.equal()
    
    if not eq:
        changes = diff.diff()
        pprint(changes)
        
    sys.exit(0 if eq else 1)

if __name__ == "__main__":
    main()
