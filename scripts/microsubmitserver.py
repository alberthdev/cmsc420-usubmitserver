#!/usr/bin/env python
# TODO:
#   * add notification of updated build server
#     (via https://github.com/js-cookie/js-cookie +
#     http://opengameart.org/content/alertmenu-effect +
#     HTML5 desktop notifications?)
#   * zipped cache (instead of individual downloads)
# 
# Resulting files:
#   NNNNN.xml.output.xml - resulting output
#   NNNNN.xml.ci.error - error code (ONLY if error occurred)
#   NNNNN.xml.ci.diff - diff (if any)
#   NNNNN.xml.ci.errordiff - error code on diff
# Historical log:
#   build_ci.csv

# Data structure:
# commits (ordered by date)
# [
#   {
#     hash, date,
#     tests: {
#              test_id: { result, errcode, differrcode, output }
#            }
#   }
# ]

# CSV format:
# hash, date, TestBundle1, TestBundle2, ...
#   TestBundle: test_id|RunTrue/False|DiffTrue/False|RunErrCode|DiffErrCode|RunOutput|DiffOutput|ImageBundle|HTMLBundle
#        [idx]: 0      |1            |2             |3         |4          |5        |6         |7          |8
#     ImageBundle: imgname\B64ENC;imgname\B64ENC;...
#     HTMLBundle: htmlname\B64ENC;htmlname\B64ENC;...

import csv
import glob
import shutil
import os
import sys
import subprocess
import traceback
import base64
import zlib
import html
import errno
import urllib.parse

from urllib import request
from io import StringIO
from jinja2 import Environment, FileSystemLoader
from configparser import ConfigParser

def get_set_config(parser, key, default_value, default_value_saved = True):
    if not parser.has_section("config"):
        parser.add_section("config")
    
    if type(default_value) == bool:
        found_value = parser.getboolean("config", key, fallback=default_value)
    else:
        found_value = parser.get("config", key, fallback=default_value)
    
    if default_value_saved:
        parser.set("config", key, str(found_value))
    else:
        if parser.has_option("config", key):
            parser.remove_option("config", key)
    
    return found_value

gsc = get_set_config

# Change to script dir
SCRIPT_DIR = os.path.dirname(sys.argv[0])
os.chdir(SCRIPT_DIR if SCRIPT_DIR else ".")

# Configuration file
parser = ConfigParser()
p = parser
try:
    parser.read("microsubmitserver.ini", encoding="utf-8")
except configparser.ParsingError as err:
    print("Could not parse configuration file:", err)

SINGLE_COMMIT_MODE = gsc(p, "single_commit_mode", False)
LOCAL_MODE = gsc(p, "local_mode", False)
PROJECT_BASE_URL = gsc(p, "project_base_url", "https://gitlab.com/USERNAME/cmsc420-project-repo")
WEB_ROOT_URL = gsc(p, "web_root_url", "https://USERNAME.gitlab.io/cmsc420-project-repo")
REL_ROOT_URL = ("/" if LOCAL_MODE else gsc(p, "rel_root_url", urllib.parse.urlparse(WEB_ROOT_URL).path, False))
BUILD_CI_CSV_URL = gsc(p, "build_ci_csv_url", WEB_ROOT_URL + "/build_ci.csv", False)
DIFF_BASE_URL = gsc(p, "diff_base_url", WEB_ROOT_URL + "/diff", False)

# Save config file
try:
    fh = open("microsubmitserver.ini", "w")
    parser.write(fh)
    fh.close()
except Exception as err:
    print("Could not write configuration file:", err)

# Add trailing slash to relative root URL
if REL_ROOT_URL[-1] != "/":
    REL_ROOT_URL += "/"

TESTS_DIR = os.path.sep.join([ "..", "tests" ])
RESULTS_DIR = os.path.sep.join([ "..", "results" ])

def mkdir_p(d):
    if not os.path.isdir(d):
        os.mkdir(d)

# From http://stackoverflow.com/a/15063941/1094484
def set_csv_limits():
    maxInt = sys.maxsize
    decrement = True
    decTimes = 0

    while decrement:
        # decrease the maxInt value by factor 10 
        # as long as the OverflowError occurs.

        decrement = False
        try:
            csv.field_size_limit(maxInt)
        except OverflowError:
            maxInt = int(maxInt/10)
            decrement = True
            decTimes += 1
    
    print("CSV limits adjusted to: " + str(maxInt) + " (reduced " + str(decTimes) + " times)")

def render_html(template_file, target, **vars):
    env = Environment(loader=FileSystemLoader('../html/'))
    template = env.get_template(template_file)
    output = template.render(**vars)
    
    fh = open(target, "w")
    fh.write(output)
    fh.close()

def b64zlib_encode(s):
    return base64.b64encode(zlib.compress(s.encode('ascii'), 9)).decode()

def b64zlib_decode(s):
    return zlib.decompress(base64.b64decode(s)).decode()

def download(url, destination):
    tries = 0
    
    while 1:
        try:
            response = request.urlopen(url)
            file_str = response.read().decode("utf-8")
            file_dst = open(destination, "w")
            file_dst.write(file_str)
            file_dst.close()
            break
        except:
            if tries < 3:
                tries += 1
            else:
                raise

def main():
    commits = []
    commits_list = []
    unified_tests = []
    
    json_avail = {}
    
    set_csv_limits()
    
    # Create public + public/diff dir
    mkdir_p("../public")
    mkdir_p("../public/diff/")
    
    # Get latest git log
    single_commit_info = subprocess.check_output("git log --name-status -1".split(" ")).strip().decode()
    
    # Count number of commits
    commit_count = subprocess.check_output("git rev-list --count master".split(" ")).strip().decode()
    
    if commit_count.isdigit():
        commit_count = int(commit_count)
    else:
        print("WARNING: Commit count could not be found, setting to zero. Commit information may be truncated.")
    
    # Check for poison pill (history wipe)
    if "ERASE_SUBMIT_SERVER_HISTORY" in single_commit_info:
        print("Detected poison pill, not loading or importing any CSV history data.")
    elif SINGLE_COMMIT_MODE:
        print("Single commit mode enabled, not loading or importing any CSV history data.")
    else:
        # Get CSV and import our data!
        try:
            print("Attempting to retrieve and import CSV file...")
            response = request.urlopen(BUILD_CI_CSV_URL)
            csv_input = response.read().decode("utf-8")
            buf = StringIO(csv_input)
            reader = csv.reader(buf)
            for line in reader:
                commit_dict = {}
                commit_dict["hash"] = line[0]
                commit_dict["date"] = line[1]
                
                print(" -> Found commit: %s" % commit_dict["hash"])
                
                if (commit_dict["hash"] in commits_list):
                    continue
                
                commits_list.append(commit_dict["hash"])
                commit_dict["tests"] = {}
                
                if len(line) > 2:
                    for test_csv in line[2:]:
                        test_sp = test_csv.replace("\\n", "").strip().split("|")
                        test_id = test_sp[0]
                        test_runok = True if test_sp[1] == "True" else False
                        test_diffok = True if test_sp[2] == "True" else False
                        test_errcode = int(test_sp[3])
                        test_differrcode = int(test_sp[4])
                        test_output = b64zlib_decode(test_sp[5])
                        test_diffoutput = b64zlib_decode(test_sp[6])
                        test_imgs = {}
                        test_pages = {}
                        
                        if (len(test_sp) >= 8) and (test_sp[7].strip() != ""):
                            for img_entry in test_sp[7].split(";"):
                                img_entry_sp = img_entry.split("\\")
                                test_imgs[img_entry_sp[0]] = b64zlib_decode(img_entry_sp[1])
                        
                        if (len(test_sp) >= 9) and (test_sp[8].strip() != ""):
                            for page_entry in test_sp[8].split(";"):
                                page_entry_sp = page_entry.split("\\")
                                test_pages[page_entry_sp[0]] = b64zlib_decode(page_entry_sp[1])
                        
                        commit_dict["tests"][test_id] = {}
                        commit_dict["tests"][test_id]["run_result"] = test_runok
                        commit_dict["tests"][test_id]["diff_result"] = test_diffok
                        commit_dict["tests"][test_id]["errcode"] = test_errcode
                        commit_dict["tests"][test_id]["differrcode"] = test_differrcode
                        commit_dict["tests"][test_id]["output"] = test_output
                        commit_dict["tests"][test_id]["diffoutput"] = test_diffoutput
                        commit_dict["tests"][test_id]["imgs"] = test_imgs
                        commit_dict["tests"][test_id]["pages"] = test_pages
                
                commits.append(commit_dict)
            
            # Attempt to download JSONs
            print("Attempting to download JSONs for diffing...")
            for commit in commits:
                json_avail[commit["hash"]] = {}
                print(" -> Downloading JSON outputs for commit %s..." % commit["hash"])
                mkdir_p("../public/diff/%s" % commit["hash"])
                for test_id in commit["tests"]:
                    json_avail[commit["hash"]][test_id] = {}
                    try:
                        download(DIFF_BASE_URL + "/%s/%s.output.xml.json" % (commit["hash"], test_id),
                            "../public/diff/%s/%s.output.xml.json" % (commit["hash"], test_id))
                        print("    -> Retrieved original JSON file for test %s, continuing..." % test_id)
                        json_avail[commit["hash"]][test_id]["original"] = True
                    except:
                        print("    -> Unable to retrieve original JSON file for test %s (%s), continuing..." % (test_id, commit["hash"]))
                        print("       URL: %s" % (DIFF_BASE_URL + "/%s/%s.output.xml.json" % (commit["hash"], test_id)))
                        json_avail[commit["hash"]][test_id]["original"] = False
                    
                    try:
                        download(DIFF_BASE_URL + "/%s/%s.input.xml.ci.output.xml.json" % (commit["hash"], test_id),
                            "../public/diff/%s/%s.input.xml.ci.output.xml.json" % (commit["hash"], test_id))
                        print("    -> Retrieved generated JSON file for test %s, continuing..." % test_id)
                        json_avail[commit["hash"]][test_id]["generated"] = True
                    except:
                        print("    -> Unable to retrieve generated JSON file for test %s (%s), continuing..." % (test_id, commit["hash"]))
                        print("       URL: %s" % (DIFF_BASE_URL + "/%s/%s.input.xml.ci.output.xml.json" % (commit["hash"], test_id)))
                        json_avail[commit["hash"]][test_id]["generated"] = False
                try:
                    os.rmdir("../public/diff/%s" % commit["hash"])
                    print(" -> No JSON outputs for commit %s found, removing directory..." % commit["hash"])
                except OSError as e:
                    if e.errno != errno.ENOTEMPTY:
                        print("Strange error occurred while trying to delete commit diff dir...")
                        traceback.print_exc()
        except:
            print("Unable to retrieve/import CSV file, continuing...")
            traceback.print_exc()
    
    ## BUILD COMMIT DICT
    print("Processing current commit results...")
    commit_dict = {}

    commit_dict["hash"] = subprocess.check_output("git rev-parse --short HEAD".split(" ")).strip().decode()
    commit_dict["date"] = subprocess.check_output("git show -s --format=%ci".split(" ")).strip().decode()
    
    # Fetch latest commit info
    if len(commits) > 0:
        # Check if the latest commit is our current commit!
        if commit_dict["hash"] == commits[0]["hash"]:
            # Fetch from the last last commit
            latest_commit_info = subprocess.check_output(("git log --name-status "+commits[1]["hash"]+"..HEAD").split(" ")).strip().decode()
        else:
            # Fetch from last commit
            latest_commit_info = subprocess.check_output(("git log --name-status "+commits[0]["hash"]+"..HEAD").split(" ")).strip().decode()
    else:
        # Only fetch latest commit
        latest_commit_info = subprocess.check_output("git log --name-status -1".split(" ")).strip().decode()

    if not (commit_dict["hash"] in commits_list):
        print(" -> Processing current commit test results...")
        commits_list.append(commit_dict["hash"])

        commit_dict["tests"] = {}; print(os.path.join(TESTS_DIR, "*.input.xml")); print(os.getcwd())

        # Check files + append new test results
        for filename in glob.glob(os.path.join(TESTS_DIR, "*.input.xml")):
            b_filename = os.path.basename(filename)
            test_id = b_filename.replace(".xml", "")
            test_id = test_id.replace(".input", "")
            
            print("    -> Processing test result: %s" % test_id)
            
            output_fn  = os.path.join(RESULTS_DIR, b_filename + ".ci.output.xml")
            ci_err     = os.path.join(RESULTS_DIR, b_filename + ".ci.error")
            ci_diff    = os.path.join(RESULTS_DIR, b_filename + ".ci.diff")
            ci_errdiff = os.path.join(RESULTS_DIR, b_filename + ".ci.errordiff")
            
            ci_imgs    = os.path.join(RESULTS_DIR, b_filename + ".ci.imgs")
            ci_pages   = os.path.join(RESULTS_DIR, b_filename + ".ci.pages")
            
            run_err_code = 0
            diff_err_code = 0
            
            fh = open(output_fn)
            output = fh.read()
            fh.close()
            
            if os.path.exists(ci_err):
                fh = open(ci_err)
                run_err_code = int(fh.read())
                fh.close()
            
            fh = open(ci_diff)
            diff = fh.read()
            fh.close()
            
            if os.path.exists(ci_errdiff):
                print("    -> Diffing failed!")
                fh = open(ci_errdiff)
                diff_err_code = int(fh.read())
                fh.close()
            
            commit_dict["tests"][test_id] = {}
            commit_dict["tests"][test_id]["run_result"] = (run_err_code == 0)
            commit_dict["tests"][test_id]["diff_result"] = (diff_err_code == 0)
            commit_dict["tests"][test_id]["errcode"] = run_err_code
            commit_dict["tests"][test_id]["differrcode"] = diff_err_code
            commit_dict["tests"][test_id]["output"] = output
            commit_dict["tests"][test_id]["diffoutput"] = diff
            commit_dict["tests"][test_id]["imgs"] = {}
            commit_dict["tests"][test_id]["pages"] = {}
            
            if os.path.exists(ci_imgs):
                for img_fn in glob.glob(os.path.join(ci_imgs, "*.png")):
                    base_img_fn = os.path.basename(img_fn)
                    try:
                        img_fh = open(img_fn, "rb")
                        img_b64 = base64.b64encode(img_fh.read()).decode()
                        img_fh.close()
                        
                        commit_dict["tests"][test_id]["imgs"][base_img_fn] = img_b64
                        
                        print("    -> Including image: %s" % base_img_fn)
                    except:
                        print("Unable to convert PNG file, continuing...")
                        traceback.print_exc()
            
            if os.path.exists(ci_pages):
                for page_fn in glob.glob(os.path.join(ci_pages, "*.html")):
                    base_page_fn = os.path.basename(page_fn)
                    try:
                        page_fh = open(page_fn, "rb")
                        page_b64 = base64.b64encode(page_fh.read()).decode()
                        page_fh.close()
                        
                        commit_dict["tests"][test_id]["pages"][base_page_fn] = page_b64
                        
                        print("    -> Including HTML page: %s" % base_page_fn)
                    except:
                        print("Unable to convert HTML file, continuing...")
                        traceback.print_exc()

        # Append commit dict to front
        commits.insert(0, commit_dict)

    # Build unified tests array
    for commit in commits:
        for test_id in commit["tests"]:
            if test_id not in unified_tests:
                unified_tests.append(test_id)

    unified_tests = sorted(unified_tests)
    
    # Dump to CSV
    print("Writing results (and historical) CSV...")
    with open('../public/build_ci.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
        
        for commit in commits:
            row_arr = []
            row_arr.append(commit["hash"])
            row_arr.append(commit["date"])
            for test_id in commit["tests"]:
                row_arr.append("|".join(
                    [
                        test_id,
                        str(commit["tests"][test_id]["run_result"]),
                        str(commit["tests"][test_id]["diff_result"]),
                        str(commit["tests"][test_id]["errcode"]),
                        str(commit["tests"][test_id]["differrcode"]),
                        b64zlib_encode(commit["tests"][test_id]["output"]),
                        b64zlib_encode(commit["tests"][test_id]["diffoutput"]),
                        ";".join(["\\".join([img_fn, b64zlib_encode(commit["tests"][test_id]["imgs"][img_fn])]) for img_fn in commit["tests"][test_id]["imgs"]]),
                        ";".join(["\\".join([page_fn, b64zlib_encode(commit["tests"][test_id]["pages"][page_fn])]) for page_fn in commit["tests"][test_id]["pages"]])
                    ]
                ))
            writer.writerow(row_arr)
    
    # Install diffs from current test
    print("Installing current JSON outputs...")
    mkdir_p("../public/diff/%s" % commits[0]["hash"])
    for filename in glob.glob("../results/*.xml.json"):
        shutil.copy(filename, "../public/diff/%s/" % commits[0]["hash"])
    
    print("Indexing current JSON outputs... (%s)" % commits[0]["hash"])
    json_avail[commits[0]["hash"]] = {}
    for test_id in commits[0]["tests"]:
        json_avail[commits[0]["hash"]][test_id] = {}
        json_avail[commits[0]["hash"]][test_id]["original"] = \
            os.path.isfile("../public/diff/%s/%s.output.xml.json" % (commits[0]["hash"], test_id))
        json_avail[commits[0]["hash"]][test_id]["generated"] = \
            os.path.isfile("../public/diff/%s/%s.input.xml.ci.output.xml.json" % (commits[0]["hash"], test_id))
    
    # Build HTML
    print("Rendering results to HTML...")
    mkdir_p("../public/commits/")
    
    # Render index page
    render_html("index.html", "../public/index.html", commits=commits, unified_tests=unified_tests,
        commit_info=latest_commit_info,
        project_base_url=PROJECT_BASE_URL, root_url=WEB_ROOT_URL, rel_root_url=REL_ROOT_URL)
    
    # Render diff page
    # TODO: use unified_tests for easier test switching
    render_html("diff/index.html", "../public/diff/index.html",
        project_base_url=PROJECT_BASE_URL, root_url=WEB_ROOT_URL, rel_root_url=REL_ROOT_URL)
    
    if os.path.isdir("../public/diff/css"):
        shutil.rmtree("../public/diff/css")
    shutil.copytree("../html/diff/css", "../public/diff/css")
    
    if os.path.isdir("../public/diff/js"):
        shutil.rmtree("../public/diff/js")
    shutil.copytree("../html/diff/js", "../public/diff/js")
    
    mkdir_p("../public/static")
    
    if os.path.isdir("../public/static/js"):
        shutil.rmtree("../public/static/js")
    shutil.copytree("../html/static/js", "../public/static/js")
    
    if os.path.isdir("../public/static/css"):
        shutil.rmtree("../public/static/css")
    shutil.copytree("../html/static/css", "../public/static/css")
    
    if os.path.isdir("../public/static/fonts"):
        shutil.rmtree("../public/static/fonts")
    shutil.copytree("../html/static/fonts", "../public/static/fonts")
    
    if os.path.isdir("../public/static/img"):
        shutil.rmtree("../public/static/img")
    shutil.copytree("../html/static/img", "../public/static/img")
    
    # Render commit pages
    i = 0
    while i < len(commits):
    #for commit in commits:
        commit = commits[i]
        print(" -> Exporting commit: %s" % commit["hash"])
        mkdir_p("../public/commits/%s/" % commit["hash"])
        #print(json_avail)
        
        if i < len(commits) - 1:
            commit_info = subprocess.check_output(("git log --name-status "+commits[i+1]["hash"]+".."+commit["hash"]).split(" ")).strip().decode()
        else:
            if commit_count > 1:
                commit_info = subprocess.check_output(("git log --name-status "+commit["hash"]+"^.."+commit["hash"]).split(" ")).strip().decode()
            else:
                commit_info = subprocess.check_output(("git log --name-status -1").split(" ")).strip().decode()
        
        render_html("commit.html", "../public/commits/%s/index.html" % commit["hash"], commit=commit,
            commit_json_avail=json_avail[commit["hash"]],
            commit_info=commit_info,
            project_base_url=PROJECT_BASE_URL, root_url=WEB_ROOT_URL, rel_root_url=REL_ROOT_URL,
            sorted=sorted)
        
        for test in commit["tests"]:
            print("    -> Exporting test: %s" % test)
            render_html("output.html", "../public/commits/%s/%s-run.html" % (commit["hash"], test),
                output_type="Run", commit=commit, test=test,
                project_base_url=PROJECT_BASE_URL, root_url=WEB_ROOT_URL, rel_root_url=REL_ROOT_URL)
            render_html("output.html", "../public/commits/%s/%s-diff.html" % (commit["hash"], test),
                output_type="Diff", commit=commit, test=test, commit_test_json_avail=json_avail[commit["hash"]][test],
                project_base_url=PROJECT_BASE_URL, root_url=WEB_ROOT_URL, rel_root_url=REL_ROOT_URL)
            if len(commit["tests"][test]["pages"]) > 0:
                page_fn_base_path = "../public/commits/%s/%s/" % (commit["hash"], test)
                mkdir_p("../public/commits/%s/%s/" % (commit["hash"], test))
                for page_fn in commit["tests"][test]["pages"]:
                    page_fh = open(os.path.join(page_fn_base_path, page_fn), "w")
                    page_fh.write(base64.b64decode(commit["tests"][test]["pages"][page_fn]).decode())
                    page_fh.close()
        i += 1

if __name__ == "__main__":
    main()
