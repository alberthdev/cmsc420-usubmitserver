CMSC420 μSubmitServer
======================

Introduction
-------------
The CMSC420 μSubmitServer is a handly little tool for quickly testing
your code and getting feedback about the results. (Hence the name:
μSubmitServer!)

This tester runs through all of the input XML files, compares the
output XML files, and reports the results accordingly in a nice
looking static webpage.

This tester also tracks test results over many Git commits, allowing
you to figure out if a certain commit broke a test or not.

This is a very hacky solution I came up with during the time I was taking
CMSC420 - therefore, your mileage may vary when it comes to getting this
to work! (Code is very, very dirty...)

Demo
-----
A demo of this working can be found here:
https://alberthdev.gitlab.io/cmsc420-p1/

Screenshots
------------
More screenshots can be found [here](https://gitlab.com/alberthdev/cmsc420-usubmitserver/wikis/screenshots).

![](http://i.imgur.com/MsmPssT.png)

Prerequisites
--------------
You will need to have your project in a Git repository in order for
this to work. You do NOT need to publish your repository for this
server to work - you just need a local one created in your project
folder.

Other prerequisites to running this depends on what you need.

### I want to let GitLab do all of the hard work of testing my project!
**This is the easiest way to get your project tested without too much
pain! That said, since you are using GitLab, testing may be a bit slow.**

You'll need:

  * A text editor for setting up your configuration
  * A GitLab account and private repository to store, run, and test
    your project
  * A Git client (and knowing how to use it)

### I want to run μSubmitServer locally.
**If you have time, and want the flexibility and speed that running
it on your own computer offers, go for this method!**

You'll need:

  * A text editor for setting up your configuration
  * Python 3.5.x installed, in your PATH, with packages `deepdiff`,
    `lxml`, and `jinja2` installed

     * Hint: if you need to install those packages, run:
       `pip install deepdiff lxml jinja2`

  * Java compiler in your PATH
  * BASH interpeter

    * On Windows, you may need to use Cygwin/MinGW or even the new
      Windows 10 UNIX subsystem to get this working!
    * On Linux/Mac, you should be set as long as it's installed.
  
  * ANT or Eclipse installed
  
    * If you are using ANT, ANT should be in your PATH.

GitLab Integration Setup (Easiest)
-----------------------------------

  1. [Create a GitLab account][gl-signup], if you don't have one
     already.
  2. [Setup your GitLab SSH keys][gl-sshkeys], if you have that set
     up already.
  3. [Create a new GitLab project][gl-newproj], if you don't have
     one already on GitLab. Make sure it's private!
  4. Download the GitLab project with your Git client. Make sure to
     use the SSH URL, if you configured that.
  5. Follow the **Core Setup** instructions, then continue.
  6. Modify `scripts/microsubmitserver.ini` to reflect your GitLab
     username and project name.
  7. That's it! Commit all of the changes, and then push them to
     GitLab.
  8. Now go to your project page, and click on Pipelines. If everything
     is setup correctly, GitLab should now be building, running, and
     testing your project!
  9. After the process is completed successfully, go to
     http://USERNAME.gitlab.io/PROJECT to see your μSubmitServer!

Local Setup (Difficult)
------------------------

  1. Create a Git repository in your project folder, if you haven't
     already.
  2. Follow the **Core Setup** instructions, then continue.
  3. Modify `scripts/microsubmitserver.ini` to use the following:
     * For `web_root_url`, set this to `http://127.0.0.1:8000/`.
     * For `local_mode`, set this to `True`.
     * For `project_base_url`, set this to your GitLab project URL
        if you have one. If not, leave this alone.
  4. Follow the **Running Local Tests** instructions to build,
     run, and test your project!

Core Setup
-----------

  1. Download this repository's files, and copy them into your
     project folder. Make sure to include the dotfiles (e.g.
     `.gitlab-ci.yml`). (On Linux/Mac, these dotfiles will
     be hidden - make sure to include them!)
  2. Add or move your CMSC420 project into the src/ folder. Your
     folder structure should look like `src/cmsc420/meeshquest/`,
     etc.
  3. Edit `build/project-jar.xml` to fit your project's main method
     and your project's Git repository folder name.
  4. Download all of the test data (including any images, XSD/XSL files,
     and test XML inputs/outputs) into the `tests/` folder.
  5. Download the `cmsc420util.jar` file into your `lib/` folder.
     Unless anything has changed, this file can be found [here][420pf].
  6. That's it! If you came here from other steps, go back there.

Running Local Tests
--------------------

  1. Make sure you have all of the prerequisites satisfied first!
  2. Commit any changes that you have!
  3. Build your project:
     * If you are using Eclipse, right click on your
       `build/project-jar.xml`, and select **Run As > Ant Build**.
     * If you are using ANT directly, `cd` into your `build` directory,
       and run `ant -buildfile build/project-jar.xml all`.
     * This should build your JARs for testing, along with a
       submit server friendly JAR as well.
  4. Within a BASH interpreter, `cd` into your `scripts` directory, and run
     the tester: `./run_tests.sh`.
  5. While still inside your `scripts` directory, run the difference checker:
     `./run_tests_diff.sh`.
  6. Finally, while still inside your `scripts` directory, run the
     μSubmitServer: `python3 ./microsubmitserver.py`.
  7. To see the results, run `python3 ./run_static_server.py`, and go to the
     URL specified.

Running GitLab Tests
---------------------

  1. Commit any changes that you have.
  2. Push your code to GitLab.
  3. Wait a few minutes (or until you see the build/run/test process complete).
  4. Tada! You're done!

Adding Tests
-------------
The whole point of using this tool is to do more testing! You can easily add
your own testcases, or even testcases from other people's submissions on the
canonical test server!

If you have tests you want to add, simply add a test to the `tests/` folder.
You will need to have the `.input.xml` file and the canonical's `.output.xml`
file in order for the tester to work. (Example: `my.pmstresstest.input.xml`
would have my input, and `my.pmstresstest.output.xml` would have the correct
output.)

[gl-signup]: https://gitlab.com/users/sign_in
[gl-sshkeys]: https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html
[gl-newproj]: https://gitlab.com/projects/new
[420pf]: https://cs.umd.edu/users/meesh/420/ProjectBook/
